from step_36_oop_puzzles import Country
from operator import attrgetter

# Arrays Slicing
a = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
print('All elements from 0 to 9', a[0:10])
print('Elements from start to 3', a[:4])
print('Elements from 3 to end', a[3:])
print('Elements in reverse: from start to end with step -1', a[::-1])
print('Elements in reverse: from start to end with step -4', a[::-3])
del a[0:3]
print('Deleted from 0 to 2', a)
a[0:2] = ['zero', 'one', 'two']
print('replace from 0 to 2', a)
a.insert(3, ['three', 'four'])
print('a is not back!!', a)

####### sort a list
a = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
print('Sorted', sorted(a, key=len, reverse=True))
print(a)
a.sort(key=len, reverse=True)
print('a.sort()', a)

######## List as a stack or a queue
a = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
print('Popped last element', a.pop())
print('a: ', a)
print('Popped last element', a.pop(0))

######## List with classes
countries = [Country('India', 10000, 100),
             Country('Germany', 80, 88),
             Country('Egypt', 88, 99)]
print(countries)
countries.sort(key=attrgetter('population'), reverse=False)
print(max(countries, key=attrgetter('area')))
print('Sorted Countries', countries)

######## List comprehension
print('\n\n\n')
numbers_of_length_four = [number for number in a if len(number) == 4]
print('Number with length four', numbers_of_length_four)

######## Set
number_set = {1, 2, 3, 3, 3, 4}
numbers_one_to_five = {range(1, 5)}
print('Set: ', number_set)

######## List with squares of the first 10 numbers
squares_of_1_to_10 = [number ** 2 for number in range(1, 11)]
print('squares_of_1_to_10',squares_of_1_to_10)

squares_of_1_to_10_set = {number ** 2 for number in range(1, 11)}
print(squares_of_1_to_10_set)

squares_of_1_to_10_dic = {number:number ** 2 for number in range(1, 11)}
print(squares_of_1_to_10_dic)
