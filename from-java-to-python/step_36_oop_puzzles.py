'''
1. Dynamically add attributes to an Object in runtime
#########################################################
# 2. Multiple Inheritence
#########################
3. Abstract classes and Polymorphism
4. Duck typing
'''
from abc import ABC, abstractmethod


class Country:

    def __init__(self, name, population=100, area=100):
        self.name = name;
        self.population = population
        self.area = area

    def __repr__(self):
        return repr((self.name))


class City(Country):
    def __init__(self, city_name, country):
        super().__init__(city_name)
        self.country = country

    def __repr__(self):
        return repr((self.name, ('country', self.country)))

    def get_population(self):
        return self.population


belgium = Country('Belgium')
brussels = City("Brussels", belgium)
# 1. Dynamically add attributes to an Object in runtime
belgium.population = 5000000
print(belgium, belgium.population)

print(brussels)


##### 3. Inheritence
class Animal(ABC):
    def exist(self):
        self.get_born()
        self.grow_up()
        self.start_moving()

    @abstractmethod
    def get_born(self):
        pass

    @abstractmethod
    def grow_up(self):
        pass

    @abstractmethod
    def start_moving(self):
        pass


class LandAnimals(Animal):
    def walk(self):
        print('Walk')

    def get_born(self):
        print('get born')

    def grow_up(self):
        print('get born')

    def start_moving(self):
        print('start moving')


class WaterAnimal:
    def swim(self):
        print('Swim')


class AmphibianCAnimal(LandAnimals, WaterAnimal):
    def move(self):
        super().swim()
        super().walk()


frog = AmphibianCAnimal()
frog.move()
frog.exist()


###### 4. Duck typing
class Test1:
    def execute(self):
        print("Test1")


class Test2:
    def execute(self):
        print("Test2")


tests = [Test1(), Test2()]
for test in tests: test.execute()


####### 5. Static Variables and Methods
class Player:
    count = 0

    def __init__(self, name):
        self.name = name
        Player.count += 1

    @staticmethod
    def get_count():
        return Player.count


messi = Player('Messi')
ronaldo = Player('Ronaldo')
print(Player.count)
print(Player.get_count())

####### 6. Static Methods
