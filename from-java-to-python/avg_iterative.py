from statistics import mean

x = [1,2,3,4,5,6,7,8,9,10]

print(x)

def print_square_of_numbers(n):
    for i in range(1,n):
        print(i*i)

def print_square_of_Even_numb(n):
    for i in range(2,n,2):
        print(i*i)

def print_num_in_reverse(n):
    for i in range(n,0,-1):
        print(i)


def print_each_word_in_line(str):
    words = str.split(" ")
    for word in words:
        print(word)

def calc_avg_iter(a):
    avg = 0
    n=0
    for i in range(1,len(a)+1):
        avg = avg*n/(n+1) + a[i-1]/(n+1)
        n+=1

    return avg

print(mean(x))
print(calc_avg_iter(x))
n = 10
print_square_of_numbers(n)
print()
print_square_of_Even_numb(n)
print()
print_num_in_reverse(n)

str = "This is a great word"
print_each_word_in_line(str)