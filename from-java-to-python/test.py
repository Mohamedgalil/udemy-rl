import string


def print_table(n=5, start=1, end=10):
    print("Mutl. Table \"%d\"" % n)
    for i in range(start, end + 1):
        print("%2d * %2d = %3d" % (n, i, n * i))


def print_table_formatted(n=5, start=1, end=10):
    print("Mutl. Table \"%d\"" % n)
    for i in range(start, end + 1):
        print(f"{n} * {i} = {n*i}")


print_table()
print_table(6)
print_table(9, 2, 5)
print_table(100)

print_table_formatted()

print(True)

''' Mathematical Operations'''
print(5 / 4)
print(5 // 4)

print(5 ** 3)

print(max(2, 4, 5))
print(round(5 / 4))
print(float(5 // 4))
print(True and False)

''' Find all Capital letters in string'''
print()


def find_cap_letters_string(str):
    upper_case = [x for x in str if x in string.ascii_uppercase]
    return upper_case


def find_small_letters_string(str):
    lower_case = [x for x in str if x in string.ascii_lowercase]
    return lower_case


def print_word_in_string_sep(str):
    words = str.split()
    for x in words: print(3*x)


str = "BolA BolA"
print("upper_case: ", find_cap_letters_string(str))
print("lower_case: ", find_small_letters_string(str))
print_word_in_string_sep(str)
