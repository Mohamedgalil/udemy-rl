n1 = float(input("first number:"))
n2 = float(input("second number:"))
operation = int(input("Select Operation\n1-Add\n2-Subtract\n3-Multiply\n4-Divide"))
if operation == 1:
    print(f"{n1}+{n2}={n1+n2}")
elif operation == 2:
    print(f"{n1}-{n2}={n1-n2}")
elif operation == 3:
    print(f"{n1}*{n2}={n1*n2}")
elif operation == 4:
    print(f"{n1}/{n2}={n1/n2}")

