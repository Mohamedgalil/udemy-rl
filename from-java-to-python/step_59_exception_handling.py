## Step 59 to 61: Exepction Handling
try:
    a = 1/1
    b = a + 1
except ArithmeticError:
    print('Are you dividing by zero?!')
    b = 0
else:
    print('Exception is not thrown')
    b = b/0
finally:
    print('Exception handled successfully')
print(b)
