class Book:

    def __init__(self, name="Default", num_of_copies=1):
        self.name = name
        self.num_of_copies = num_of_copies

    ''' toString, technical representation of object'''

    def __repr__(self):
        # return repr(self.name) +", "+ str(repr(self.num_of_copies))
        return repr((self.name, self.num_of_copies))

    def increase_num_of_copies(self, add_num_of_copies):
        self.num_of_copies += add_num_of_copies

    def decrease_num_of_copies(self, few_num_of_copies):
        self.num_of_copies -= few_num_of_copies


book1 = Book("Python in 100 Steps")
book1.increase_num_of_copies(5)
book2 = Book("Intro to Java")
book2.decrease_num_of_copies(1)
print(book1)
print(book2)
