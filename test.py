import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np


x = np.arange(100)
f = np.log(x)/x # when x increases , f decreases -> confidence interval UCB1
plt.plot(f)
plt.show()
