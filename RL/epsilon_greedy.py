import numpy as np
import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt


class epsilon_greedy:
    def __init__(self, epsilon):
        self.epsilon = epsilon


'''
Which Bandit maximizes reward?
Given multiple bandits, our job is to select the Bandit which maximizes the reward
Therefore, estimate the mean of each Bandit --> select the Bandit with maximum mean
'''


class Bandit:

    def __init__(self, hidden_avg):
        self.hidden_avg = hidden_avg
        self.estimated_avg = 0
        self.n = 0

    def pull_arm(self):
        return np.random.randn() + self.hidden_avg

    def update(self, x):
        self.estimated_avg = self.estimated_avg * self.n / (self.n + 1) + x / (self.n + 1)

    def __repr__(self):
        return repr((self.hidden_avg, self.estimated_avg))




if __name__ == '__main__':
    run_experiment(1.0, 2.0, 3.0, 0.1, 100000)
    run_experiment(1.0, 2.0, 3.0, 0.5, 100000)
    run_experiment(1.0, 2.0, 3.0, 0.9, 100000)
